import logo from './logo.svg';
import './App.css';
import MakeOrderComponent from './Components/MakeOrderComponent';
import MyOrdersComponent from './Components/MyOrdersComponent';
import CarComponent from './Components/CarComponent';
import CustomerComponent from './Components/CustomerComponent';
import NavigationBar from "./Components/NavigationBar";
import Footer from './Components/Footer';
import Login from './Components/Login';
import ViewOrder from './Components/ViewOrder';
import UpdateOrder from './Components/UpdateOrder';


import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  Outlet,
  Switch,
  useParams
} from 'react-router-dom';

function App() {

  
  return (
    <div>
      <Router>
        <NavigationBar />
        <div className="container">
        <Switch>
                <Route exact path='/' component={CarComponent} />
                <Route path='/myorders' component={MyOrdersComponent} />
                <Route path='/makeorder/:id' component={MakeOrderComponent} />
                <Route path='/login' component={Login} />
                <Route path='/vieworder/:id' component={ViewOrder} />
                <Route path='/updateorder/:id' component={UpdateOrder} />
                <Route render={() => <h1>Not found!</h1>} />
            </Switch>
        </div>
        <Footer />
        </Router>
 
    </div>
  );
  
}

export default App;