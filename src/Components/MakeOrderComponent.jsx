import React, { Component } from 'react'
import BookingService from '../Services/UserService/BookingService';
import CarService from '../Services/CarService/CarService';
import "./../Style.css";
import moment from "moment";




class MakeOrderComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {    
            carId: this.props.match.params.id,
            car: {}
        }

        this.changeStartDateHandler = this.changeStartDateHandler.bind(this);
        this.changeEndDateHandler = this.changeEndDateHandler.bind(this);
        this.saveBooking = this.saveBooking.bind(this);
        this.getCar();
    }



    saveBooking = (e) => {
        e.preventDefault();

        var days = new Date(this.state.endDate) - new Date(this.state.startDate);
        const diffInDays = days / (1000 * 60 * 60 * 24);
        var price = this.state.car.carType.price * diffInDays;
        
        let booking = {sum: price.toFixed(2), startDate: this.state.startDate, endDate: this.state.endDate ,creationDate: moment().format("YYYY-MM-DD"), carId: this.state.carId};
        BookingService.createBooking(booking);
    }

    changeStartDateHandler= (event) => {
        this.setState({startDate: event.target.value});
    }

    changeEndDateHandler= (event) => {
        this.setState({endDate: event.target.value});
    }

    getCar(){
        CarService.getCarById(this.state.carId).then((res) => {
            this.setState({ car: res.data});
        })
    }

    cancel(){
        this.props.history.push('/');
    }

    



    render() {
        return (
            <div>
                <br/>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                <h1>Choose date</h1>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> StartDate: </label>
                                            <input placeholder="startDate" name="startDate" className="form-control" 
                                                value={this.state.startDate} onChange={this.changeStartDateHandler} type="date"/>
                                        </div>
                                        <div className = "form-group">
                                            <label> EndDate: </label>
                                            <input placeholder="endDate" name="endDate" className="form-control" 
                                                value={this.state.endDate} onChange={this.changeEndDateHandler} type="date"/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveBooking}>Order</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default MakeOrderComponent
