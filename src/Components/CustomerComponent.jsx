import React, { Component } from 'react'
import "./../Style.css";
import CustomerService from '../Services/UserService/CustomerService';

class CustomerComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
                customers: []
        }
    }

    componentDidMount(){
        CustomerService.getCustomers().then((res) => {
            this.setState({ customers: res.data});
        });
    }

    sortByNameDown() {
        this.setState({customers: this.state.customers.sort(function(a,b){
          return b.name.localeCompare(a.name);
         })})
    }
  
    sortByNameUp() {
        this.setState({customers: this.state.customers.sort(function(a,b){
          return a.name.localeCompare(b.name);
         })})
    }
  
  
  
  sortByEmailDown() {
      this.setState({customers: this.state.customers.sort(function(a,b){
          return b.email.localeCompare(a.email);
       })})
  }
  
  sortByEmailUp() {
      this.setState({customers: this.state.customers.sort(function(a,b){
        return a.email.localeCompare(b.email);
       })})
  }

  sortByMobileDown() {
    this.setState({customers: this.state.customers.sort(function(a,b){
      return b.mobileNumber.localeCompare(a.mobileNumber);
     })})
}

sortByMobileUp() {
    this.setState({customers: this.state.customers.sort(function(a,b){
      return a.mobileNumber.localeCompare(b.mobileNumber);
     })})
}
sortByCityDown() {
    this.setState({customers: this.state.customers.sort(function(a,b){
      return b.address.city.localeCompare(a.address.city);
     })})
}

sortByCityUp() {
    this.setState({customers: this.state.customers.sort(function(a,b){
      return a.address.city.localeCompare(b.address.city);
     })})
}

    render() {
        return (
            <div>
            <h2 className="text-center">Customers</h2>
            <div class="col-12 p-3 mt-3">
            <button
            className="btn btn-outline-success btn-sm"
            onClick={(e) => this.sortByNameUp()}> Sort by Name DESC </button>
                      <button
            className="btn btn-outline-warning btn-sm"
            onClick={(e) => this.sortByNameDown()}> Sort by Name ASC </button>
                                <button
            className="btn btn-outline-info btn-sm"
            onClick={(e) => this.sortByEmailUp()}> Sort by Email ASC </button>
                                            <button
            className="btn btn-outline-secondary btn-sm"
            onClick={(e) => this.sortByEmailDown()}> Sort by Email DESC </button>
                      <button
            className="btn btn-outline-success btn-sm"
            onClick={(e) => this.sortByMobileDown()}> Sort by Mobile DESC </button>
                      <button
            className="btn btn-outline-warning btn-sm"
            onClick={(e) => this.sortByMobileUp()}> Sort by Mobile ASC </button>
                                <button
            className="btn btn-outline-info btn-sm"
            onClick={(e) => this.sortByCityUp()}> Sort by City ASC </button>
                                            <button
            className="btn btn-outline-secondary btn-sm"
            onClick={(e) => this.sortByCityDown()}> Sort by City DESC </button>
            <div class="table-responsive-sm">
                   <table class = "table table-striped table-bordered table-hover">
                            <thead align='center'>
                                <tr>
                                    <th> Name</th>
                                    <th> Email</th>
                                    <th> Mobile</th>
                                    <th> City</th>
                                </tr>
                            </thead>
                            <tbody align='center'>
                                {
                                    this.state.customers.map(
                                        customer => 
                                        <tr key = {customer.id}>
                                             <td> {customer.name} </td>   
                                             <td> {customer.email}</td>
                                             <td> {customer.mobileNumber} </td>
                                             <td> {customer.address.city} </td>  
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                 </div>

            </div>
            </div>
        )
    }
}

export default CustomerComponent
