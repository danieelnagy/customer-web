import React, { Component } from 'react';
import BookingService from '../Services/UserService/BookingService';
import CustomerService from '../Services/UserService/CustomerService';
import CarService from '../Services/CarService/CarService';


class ViewOrder extends Component {


    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            booking: {},
            car: {},
            user:{}
        }
        this.getCar();
        this.getUser();
    }



    componentDidMount(){
        BookingService.getBookingById(this.state.id).then( res => {
            this.setState({booking: res.data});
        })
    }


    getUser()  {
        CustomerService.getUserById(this.state.booking.userId).then((res) => {
            this.setState({ user: res.data});
        })
    }

    getCar(){
        CarService.getCarById(this.state.booking.carId).then((res) => {
            this.setState({ car: res.data});
        })
    }




    render() {

        return (
            <div>
            <br></br>
            <div className = "card col-md-6 offset-md-3">
                <h3 className = "text-center"> View Employee Details</h3>
                <div className = "card-body">
                    <div className = "row">
                        <label> Price: </label>
                        <div> { this.state.booking.sum }</div>
                    </div>
                    <div className = "row">
                        <label> Car: </label>
                        <div> { this.state.car.name }</div>
                    </div>
                    <div className = "row">
                        <label> StartDate: </label>
                        <div> { this.state.booking.startDate }</div>
                    </div>
                    <div className = "row">
                        <label> EndDate: </label>
                        <div> { this.state.booking.endDate }</div>
                    </div>
                    <div className = "row">
                        <label> CreationDate: </label>
                        <div> { this.state.booking.creationDate }</div>
                    </div>
                </div>

            </div>
        </div>
        );
    }
}

export default ViewOrder;