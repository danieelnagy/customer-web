import React, { Component } from 'react'
import BookingService from '../Services/UserService/BookingService';
import CarService from '../Services/CarService/CarService';
import moment from "moment";
import "./../Style.css";


class UpdateOrder extends Component {

    constructor(props) {
        super(props)

        this.state = {    
            bookingId: this.props.match.params.id
        }

        this.changeStartDateHandler = this.changeStartDateHandler.bind(this);
        this.changeEndDateHandler = this.changeEndDateHandler.bind(this);
        this.updateBooking = this.updateBooking.bind(this);

    }



    updateBooking = (e) => {
        e.preventDefault();
        let booking = {startDate: this.state.startDate, endDate: this.state.endDate ,creationDate: moment().format("YYYY-MM-DD")};
        BookingService.updateBooking(booking, this.state.bookingId);
    }

    changeStartDateHandler= (event) => {
        this.setState({startDate: event.target.value});
    }

    changeEndDateHandler= (event) => {
        this.setState({endDate: event.target.value});
    }

    cancel(){
        this.props.history.push('/');
    }

    



    render() {
        return (
            <div>
                <br/>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                <h1>Choose date</h1>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> StartDate: </label>
                                            <input placeholder="startDate" name="startDate" className="form-control" 
                                                value={this.state.startDate} onChange={this.changeStartDateHandler} type="date"/>
                                        </div>
                                        <div className = "form-group">
                                            <label> EndDate: </label>
                                            <input placeholder="endDate" name="endDate" className="form-control" 
                                                value={this.state.endDate} onChange={this.changeEndDateHandler} type="date"/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.updateBooking}>Update</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default UpdateOrder
