import React, { Component } from 'react'
import BookingService from '../Services/UserService/BookingService';
import CarService from '../Services/CarService/CarService';
import "./../Style.css";
import moment from "moment";


class MyOrdersComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            bookings: [],
            car: []
        }
    }

    componentDidMount() {
        BookingService.getMyBookings().then((res) => {
            this.setState({ bookings: res.data });
        });
    }

    viewOrder(id) {
        this.props.history.push(`/vieworder/${id}`);
    }

    updateOrder(id) {
        this.props.history.push(`/updateorder/${id}`);
    }


    sortByPriceDown() {
        this.setState({
            bookings: this.state.bookings.sort(function (a, b) {
                return b.sum - a.sum;
            })
        })
    }

    sortByPriceUp() {
        this.setState({
            bookings: this.state.bookings.sort(function (a, b) {
                return a.sum - b.sum;
            })
        })
    }


    render() {
        console.log(moment().format("YYYY-MM-DD"));
        return (
            <div>
                <h2 className="text-center">My orders</h2>
                <div class="col-12 p-3 mt-3">
                    <button
                        className="btn btn-outline-success btn-sm"
                        onClick={(e) => this.sortByPriceUp()}> Sort by Name ASC </button>
                    <button
                        className="btn btn-outline-warning btn-sm"
                        onClick={(e) => this.sortByPriceDown()}> Sort by Name DESC </button>
                    <div class="table-responsive-sm">
                        <table class="table table-striped table-bordered table-hover">
                            <thead align='center'>
                                <tr>
                                    <th>Active</th>
                                    <th> Totalsum</th>
                                    <th> Start date</th>
                                    <th> End date</th>
                                    <th> Created at</th>
                                </tr>
                            </thead>
                            <tbody align='center'>
                                {

                                    this.state.bookings.map(
                                        booking =>
                                            <tr key={booking.id}>
                                                <td>{(() => {


                                                    if (booking.endDate > moment().format("YYYY-MM-DD")
                                                        || booking.endDate === moment().format("YYYY-MM-DD")) {
                                                        return (<p>Active</p>
                                                        )
                                                    }
                                                    else {
                                                        return (
                                                            <div>Not-Active</div>
                                                        )
                                                    }

                                                })()}</td>
                                                <td> {booking.sum}</td>
                                                <td> {booking.startDate}</td>
                                                <td> {booking.endDate}</td>
                                                <td> {booking.creationDate}</td>
                                                <td>

                                                    <div>
                                                        {(() => {


                                                            if (booking.endDate > moment().format("YYYY-MM-DD")
                                                                || booking.endDate === moment().format("YYYY-MM-DD")) {
                                                                return (
                                                                    <button
                                                                        onClick={() => this.updateOrder(booking.id)}
                                                                        className="btn btn-primary btn-sm text-white w-30"
                                                                    >
                                                                        Update{" "}
                                                                    </button>
                                                                )
                                                            }
                                                        })()}
                                                    </div>


                                                    <button
                                                        onClick={() => this.viewOrder(booking.id)}
                                                        className="btn btn-success btn-sm text-white w-30"
                                                    >
                                                        ViewOrder{" "}
                                                    </button>
                                                </td>
                                            </tr>

                                    )
                                }
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        )
    }
}

export default MyOrdersComponent
