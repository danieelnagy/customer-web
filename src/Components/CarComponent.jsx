import React, { Component } from "react";
import CarService from "../Services/CarService/CarService";
import "./../Style.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  Outlet,
  useParams,
} from "react-router-dom";


class CarComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cars: [],
    };

    
    this.editCar = this.editCar.bind(this);
    this.deleteCar = this.deleteCar.bind(this);
    
  }


  componentDidMount() {
    this.getData();
  }

  getData = () => {
    CarService.getCars()
      .then((response) => {
        this.setState({ cars: response.data });
      })
      .catch((error) => {
        // handle errors here
      });
  };

  deleteCar(id) {
    CarService.deleteCar(id);
    window.location.reload();
  }

  editCar(id) {
    this.props.history.push(`/makeorder/${id}`);
  }

  sortByNameDown() {
      this.setState({cars: this.state.cars.sort(function(a,b){
        return b.name.localeCompare(a.name);
       })})
      console.log(this.state.cars);
  }

  sortByNameUp() {
      this.setState({cars: this.state.cars.sort(function(a,b){
        return a.name.localeCompare(b.name);
       })})
  }



sortByTypeDown() {
    this.setState({cars: this.state.cars.sort(function(a,b){
        return b.carType.typeName.localeCompare(a.carType.typeName);
     })})
}

sortByTypeUp() {
    this.setState({cars: this.state.cars.sort(function(a,b){
      return a.carType.typeName.localeCompare(b.carType.typeName);
     })})
}


  render() {
    return (
      <div>
        <h2 className="text-center">Cars</h2>
        <div className="row">
        </div>
        <div class="col-12 p-3 mt-3">
          <button
            className="btn btn-outline-success btn-sm"
            onClick={(e) => this.sortByNameDown()}> Sort by Name DESC </button>
                      <button
            className="btn btn-outline-warning btn-sm"
            onClick={(e) => this.sortByNameUp()}> Sort by Name ASC </button>
                                <button
            className="btn btn-outline-info btn-sm"
            onClick={(e) => this.sortByTypeUp()}> Sort by Type ASC </button>
                                            <button
            className="btn btn-outline-secondary btn-sm"
            onClick={(e) => this.sortByTypeDown()}> Sort by Type DESC </button>
          <div class="table-responsive-sm">
            <table class="table table-striped table-bordered table-hover">
              <thead align="center">
                <tr>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Color</th>
                  <th>Type</th>
                  <th>Price</th>
                  <th>Edit</th>
                </tr>
              </thead>
              <tbody align="center">
                {this.state.cars.map((car) => (
                  <tr key={car?.id}>
                    <td> {car.name} </td>
                    <td>{car.status.toString()} </td>
                    <td>{car.color} </td>
                    <td>{car.carType.typeName} </td>
                    <td>{car.carType.price + " SEK"} </td>
                    <td>
                    {(() => {


if (!car.status) {
    return (
        <button
        onClick={() => this.editCar(car.id)}
        className="btn btn-primary btn-sm text-white w-30"
      >
        Order the car{" "}
      </button>
    )
}
})()}

                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default CarComponent;
