import axios from 'axios';


const EMPLOYEE_API_BASE_URL = "http://localhost:8088/api/v1/customers"
const API_GETALL_MYBOOKING = "http://localhost:8088/api/v1/myorders"
const API_CREATE_BOOKING = "http://localhost:8088/api/v1/ordercar"
const API_GET_USER_ID = "http://localhost:8088/api/v1/findUserById"
const API_DELETE_CAR_ID = "http://localhost:8088/api/v1/deletecar"
const API_UPDATE_CAR_ID = "http://localhost:8088/api/v1/updatecar" 

class CustomerService {

    getCustomers() {
        
        return axios.get(EMPLOYEE_API_BASE_URL, {
            // Axios looks for the `auth` option, and, if it is set, formats a
            // basic auth header for you automatically.
            auth: {
              username: 'admin',
              password: 'admin'
            }
          });
    }

    getUserById(id){
      return axios.get(API_GET_USER_ID + '/' + id);
  }

}

export default new CustomerService() 