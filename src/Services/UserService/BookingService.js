import axios from 'axios';



const API_GETALL_MYBOOKING = "http://localhost:8088/api/v1/myorders"
const API_CREATE_BOOKING = "http://localhost:8088/api/v1/ordercar"
const API_GET_BOOKING_ID = "http://localhost:8088/api/v1/findOrderById"
const API_DELETE_CAR_ID = "http://localhost:8088/api/v1/deletecar"
const API_UPDATE_BOOKING_ID = "http://localhost:8088/api/v1/updateorder" 

class CustomerService {

    getMyBookings() {
        
        return axios.get(API_GETALL_MYBOOKING);
    }

    createBooking(booking){
        axios.post(API_CREATE_BOOKING, {
          sum : booking.sum,
          carId: booking.carId,
          userId: 1,
          startDate: booking.startDate,
          endDate: booking.endDate,
          creationDate: booking.creationDate
        })
        return window.location.href='/';
    }
  
  
  
  
    updateBooking(booking, bookingId){
      axios.put(API_UPDATE_BOOKING_ID + '/' + bookingId, booking);
      return window.location.href='/';
  
  }
  
    getBookingById(id){
        return axios.get(API_GET_BOOKING_ID + '/' + id);
    }
  
    deleteCar(carId){
        axios.delete(API_DELETE_CAR_ID + '/' + carId);
        }

}

export default new CustomerService() 