import axios from 'axios';


const API_GET_CARS = "http://localhost:8088/api/v1/cars"
const API_CREATE_CAR = "http://localhost:8088/api/v1/addcar"
const API_GET_CAR_ID = "http://localhost:8088/api/v1/findcarById"
const API_DELETE_CAR_ID = "http://localhost:8088/api/v1/deletecar"
const API_UPDATE_CAR_ID = "http://localhost:8088/api/v1/updatecar" 

class CarService {

    getCars() {
        
        return axios.get(API_GET_CARS, {
            // Axios looks for the `auth` option, and, if it is set, formats a
            // basic auth header for you automatically.
            auth: {
              username: 'admin',
              password: 'admin'
            }
          });
    }

    

  createCar(car){
      axios.post(API_CREATE_CAR, {
        name : car.name,
        status: true,
        color: car.color,
        carType: {
            "id": car.carTypeId
        }   
      })
      return window.location.href='/';
  }




  updateCar(car, carId){
    axios.put(API_UPDATE_CAR_ID + '/' + carId, car);

}

  getCarById(carId){
      return axios.get(API_GET_CAR_ID + '/' + carId);
  }

  deleteCar(carId){
      axios.delete(API_DELETE_CAR_ID + '/' + carId);
      }

  

}

export default new CarService() 